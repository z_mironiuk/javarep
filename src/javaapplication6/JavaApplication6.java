package javaapplication6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.TreeSet;

public class JavaApplication6 
{
    public static void metoda()
    {
        Random r = new Random();
        double pk = 0.5;    
        int n=8;
        int [][] tab = new int[r.nextInt(10)+2][n];
        //int [][] tab = new int[2][n];
        int [][] pary;
        int [][] tab2;
        ArrayList<Integer> list = new ArrayList<>();
        double [] pr = new double[tab.length];
        int pom =0;
        for(int i=0; i<tab.length; i++)
        {
            for(int j=0; j<tab[i].length; j++)
            {
                tab[i][j] = r.nextInt(2);
            }
        }
        System.out.println(Arrays.deepToString(tab));
        
        for(int i=0; i<pr.length; i++)
        {
            pr[i] = r.nextDouble();
            if(pr[i]<pk)
            {
                list.add(i);
            }
        }
        
        for(Integer i: list)
        {
            System.out.println("Wylosowany index = " + i);
        }
        
        if(list.size()%2==1)
        {
            list.remove(r.nextInt(list.size()));
        }
        for(Integer i: list)
        {
            System.out.println("Wylosowany 2x index = " + i);
        }
        System.out.println(Arrays.toString(pr));
        
        pary = new int[list.size()/2][2];
        System.out.println("il par = " + pary.length);
        for(int i=0; i<pary.length; i++)
        {
            int rr = r.nextInt(list.size());
            pary[i][0] = list.get(rr);
            list.remove(rr);
            
            rr = r.nextInt(list.size());
            pary[i][1] = list.get(rr);
            list.remove(rr);
        }
        
        System.out.println("Pary = " + Arrays.deepToString(pary));
        
        
//        int punkt1 = r.nextInt(tab[0].length-2)+1;
//        int punkt2 = r.nextInt(tab[0].length-2)+1;
        int il_pkt = r.nextInt(tab[0].length-2)+1;
        System.out.println("Wylosowana ilosc pkt = " + il_pkt);
        TreeSet<Integer> pomlist = new TreeSet<>();
        while(pomlist.size()<il_pkt)
        {
            pomlist.add(r.nextInt(tab[0].length-2)+1);
        }
        ArrayList<Integer> punkty = new ArrayList<>(pomlist);
        for(Integer i : punkty)
        {
            System.out.println("Wylosowany pkt = " + i);
        }
        tab2=new int[tab.length][tab[0].length];
        int licznik =0;
        for(int i=0; i<pary.length; i++)
        {
            for(int j=0; j<tab2[i].length; j++)
            {
                 if(punkty.contains(j))
                     licznik++;
                 if(licznik%2==0)
                 {
                      tab2[pary[i][0]][j] = tab[pary[i][0]][j];
                      tab2[pary[i][1]][j] = tab[pary[i][1]][j];
                 }
                 else
                 {
                     tab2[pary[i][0]][j] = tab[pary[i][1]][j];
                    tab2[pary[i][1]][j] = tab[pary[i][0]][j];
                 }
                     
            }
            licznik = 0;
        }
        
        System.out.println(Arrays.deepToString(tab2));

        
        
        
        
        
    }
    public static void main(String[] args) 
    {
        metoda();
    }
    
}
